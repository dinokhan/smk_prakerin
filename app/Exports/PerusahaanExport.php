<?php

namespace App\Exports;
namespace App\Exports;

use App\Perusahaan;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PerusahaanExport implements FromCollection, WithHeadings, WithMapping
{
	public function headings(): array
    {
        return [
            'Nama Perusahaan',
            'Pemilik Perusahaan',
            'Bidang Usaha',
            'Telepon',
            'Alamat',
            'Kuota Peserta',
            'Dibuat'

        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Perusahaan::all();
    }

    public function map($peserta): array
    {
        return [
            $peserta->nama_perusahaan,
            $peserta->pemilik_perusahaan,
            $peserta->bidang_usaha,
            $peserta->tlp_perusahaan,
            $peserta->alamat,
            $peserta->kuota,
            $peserta->created_at
        ];
    }
}
