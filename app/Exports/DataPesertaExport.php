<?php

namespace App\Exports;
namespace App\Exports;

use App\Peserta;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DataPesertaExport implements FromCollection, WithHeadings, WithMapping
{
	public function headings(): array
    {
        return [
            'NIS',
            'Nama Lengkap',
            'Tempat, Tanggal Lahir',
            'Jenis Kelamin',
            'Nomor Telepon',
            'Telp. Orang Tua',
            'Catatan Kesehatan',
            'Alamat',
            'Dibuat',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Peserta::select('nis','nama_lengkap','ttl','jk','tlp_peserta','tlp_orangtua', 'catatan_kesehatan', 'alamat', 'created_at')->get();
    }

    public function map($peserta): array
    {
        return [
            $peserta->nis,
            $peserta->nama_lengkap,
            $peserta->ttl,
            $peserta->jk,
            $peserta->tlp_peserta,
            $peserta->tlp_orangtua,
            $peserta->catatan_kesehatan,
            $peserta->alamat,
            $peserta->created_at
        ];
    }
}
