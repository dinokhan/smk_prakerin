<?php

namespace App\Exports;
namespace App\Exports;

use App\Pembimbing;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PembimbingExport implements FromCollection, WithHeadings, WithMapping
{
	public function headings(): array
    {
        return [
            'Nama Lengkap',
            'Nomor Telepon',
            'Dibuat',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Pembimbing::all();
    }

    public function map($peserta): array
    {
        return [
            $peserta->nama_lengkap,
            $peserta->tlp,
            $peserta->created_at,
        ];
    }
}
