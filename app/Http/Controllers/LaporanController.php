<?php

namespace App\Http\Controllers;

use App\Pembimbing;
use Illuminate\Http\Request;
use App\Exports\PesertaExport;
use App\Exports\DataPesertaExport;
use App\Imports\PesertaImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Peserta;
use App\User;
use App\Grup;
use DB;
class LaporanController extends Controller
{
    // Kunci Layar
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('kunciAkun');
    }

    public function peserta()
    {
        $neko = Peserta::orderBy('nis','ASC')->with('pembimbing','perusahaan','grup')->get();
        return view('admin.laporan.peserta', compact('neko'));
    }
    public function absensi($id)
    {
        $kehadiran = DB::table('kehadiran')->where('student_id', $id)->get();
        $neko = Peserta::orderBy('nis','ASC')->with('pembimbing','perusahaan','grup')->get();
        return view('admin.laporan.absensi', compact('neko', 'kehadiran'));
    }
    public function pembimbing()
    {
        $neko = Pembimbing::orderBy('nama_lengkap','ASC')->with('kunjungan')->get();
//        return $neko;
        return view('admin.laporan.pembimbing', compact('neko'));
    }
    public function kunjungan($id)
    {
        $absensi = DB::table('kunjungan')->where('pembimbing_id', $id)->get();
        return view('admin.laporan.kunjungan', compact('absensi'));
    }

}
