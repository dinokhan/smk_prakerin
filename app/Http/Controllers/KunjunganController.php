<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DB;

class KunjunganController extends Controller
{
    // Kunci Layar
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('kunciAkun');
    }

    public function index()
    {
        $absensi = DB::table('kunjungan')->where('pembimbing_id', auth()->user()->id)->get();
        return view('pembimbing.kunjungan.index', compact('absensi'));
    }
    public function store(Request $request)
    {
        if($request->file('foto') != NULL){
            $canMark=true;
            $request->validate([
                'foto' => 'mimes:jpg,jpeg,png'
            ]);
            $fileMove = Storage::disk('public')->putFile('kunjungan', $request->file('foto'));
            $currentDate = date('Y-m-d');
            $absensi = DB::table('kunjungan')->where('pembimbing_id', auth()->user()->id)->get();
            foreach ($absensi as $at) {
                if ($currentDate == date('Y-m-d', strtotime($at->tgl))) {
                    $canMark = false;
                    return redirect()->back()->with('gagal', '');
                    break;
                }
            }
            if($canMark){
                DB::table('kunjungan')->insert([
                    'pembimbing_id' => auth()->user()->id,
                    'tgl' => Carbon::now(),
                    'tempat' => $request->kunjungan,
                    'jumlah' => $request->jumlah,
                    'foto' => $fileMove,

                ]);
            }
            return redirect()->back()->with('sukses', '');
        }else{
            return redirect()->back()->with('kosong', '');
        }
    }
}
