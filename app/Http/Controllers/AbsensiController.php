<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;
use Carbon\Carbon;


class AbsensiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('kunciAkun');
    }
    public function index()
    {
        $kehadiran = DB::table('kehadiran')->where('student_id', auth()->user()->id)->get();
        return view('peserta.absen.index', compact('kehadiran'));
    }

    public function store(Request $request)
    {
        // Validasi
        if($request->file('foto') != NULL){
            $canMark=true;
            $request->validate([
                'foto' => 'mimes:jpg,jpeg,png'
            ]);
            $fileMove = Storage::disk('public')->putFile('absensi', $request->file('foto'));
            $currentDate = date('Y-m-d');
            $absensi = DB::table('kehadiran')->where('student_id', auth()->user()->id)->get();
            foreach ($absensi as $at) {
                if ($currentDate == date('Y-m-d', strtotime($at->created_at))) {
                    $canMark = false;
                    return redirect()->back()->with('gagal', '');
                    break;
                }
            }
            if($canMark){
                DB::table('kehadiran')->insert([
                    'student_id' => auth()->user()->id,
                    'status' => 'hadir',
                    'kunjungan' => $request->kunjungan,
                    'rangkuman' => $request->rangkuman,
                    'foto' => $fileMove,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
            return redirect()->back()->with('sukses', '');
        }else{
            return redirect()->back()->with('kosong', '');
        }
    }
    public function izin(Request $request)
    {
        // Validasi
        if($request->file('foto') != NULL){
            $canMark=true;
            $request->validate([
                'foto' => 'mimes:jpg,jpeg,png'
            ]);
            $fileMove = Storage::disk('public')->putFile('absensi', $request->file('foto'));
            $currentDate = date('Y-m-d');
            $absensi = DB::table('kehadiran')->where('student_id', auth()->user()->id)->get();
            foreach ($absensi as $at) {
                if ($currentDate == date('Y-m-d', strtotime($at->created_at))) {
                    $canMark = false;
                    return redirect()->back()->with('gagal', '');
                    break;
                }
            }
            if($canMark){
                DB::table('kehadiran')->insert([
                    'student_id' => auth()->user()->id,
                    'status' => 'izin',
                    'rangkuman' => $request->rangkuman,
                    'foto' => $fileMove,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
            return redirect()->back()->with('sukses', '');
        }else{
            return redirect()->back()->with('kosong', '');
        }
    }
}
