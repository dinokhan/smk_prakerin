<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kunjungan extends Model
{
    protected $table = 'kunjungan';
    protected $guarded = [];

    public function pembimbing()
    {
        return $this->belongsTo(Pembimbing::class);
    }
}
