@extends('layouts.master')

@section('title','Data Peserta')

@section('css')
    <!-- DataTables -->
    <link href="{{asset('admin/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Responsive datatable examples -->
    <link href="{{asset('admin/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css"/>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Laporan Kunjungan</h4>
                    <button style="float: right;"  class="btn btn-primary " data-toggle="modal" data-target="#kunjungan">Tambah Kunjungan</button>
                    <br>
                    <br><br>
                    <div class="table-responsive">

                        <table id="datatable" class="table table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Foto Kunjungan</th>
                                <th>Tgl Kunjungan</th>
                                <th>Tempat Kunjungan</th>
                                <th>Jumlah Peserta</th>
                            </tr>
                            </thead>
                            <tbody class="table-striped">
                            @php $no = 1; @endphp
                            @foreach($absensi as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>
                                        <a target="_blank" href="{{ asset('storage/'.$row->foto) }}"><img class="d-flex mr-5 img-thumbnail thumb-lg" src="@if($row->foto != 'default.png'){{ asset('storage/'.$row->foto) }} @else {{ asset('admin/images/users/default.png') }} @endif" alt="Poto Profil"></a>
                                    </td>
                                    <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->tgl)->format('j F Y, g:i A') }}</td>
                                    <td>{{ $row->tempat }}</td>
                                    <td>{{ $row->jumlah }} peserta</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                    <!-- Modal Absensi -->
                    <hr>
                    <div class="modal fade" id="kunjungan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog  modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Silahkan Isi Kunjungan Berikut
                                        ini..</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('kunjungan.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="judul">Tempat Kunjungan</label>
                                            <input type="text" maxlength="191" class="form-control" name="kunjungan"
                                                   id="kunjungan" required="" placeholder="Masukkan Data Kunjungan">
                                        </div>
                                        <div class="form-group">
                                            <label for="judul">Jumlah Peserta</label>
                                            <input type="number" maxlength="191" class="form-control" name="jumlah"
                                                   id="jumlah" required="" placeholder="Jumlah Peserta">
                                        </div>

                                        <div class="form-group">
                                            <label for="path">Foto Hasil Kunjungan</label><br>
                                            <input type="file" class="filestyle" data-input="false"
                                                   data-buttonname="btn-secondary btn-sm" name="foto" id="path"
                                                   accept="image/*">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup
                                        </button>
                                        <button type="submit" class="btn btn-primary">Hadir</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@stop

@section('footer')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('admin/pages/datatables.init.js')}}"></script>
    <!-- Parsley js -->
    <script src="{{asset('admin/plugins/parsleyjs/parsley.min.js')}}"></script>
    <script>
        @if(Session::has('kosong'))
        alertify.error("Data Gambar tidak boleh kosong!");
        @endif
    </script>
    <script>
        @if(Session::has('gagal'))
        alertify.error("Anda sudah melakukan kunjungan hari ini!");
        @endif
    </script>
    <script>
        @if(Session::has('sukses'))
        alertify.success("Berhasil melakukan laporan kunjungan!");
        @endif
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>

    <script>
        $().DataTable();
    </script>
@stop
