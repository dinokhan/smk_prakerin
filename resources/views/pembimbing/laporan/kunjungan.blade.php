@extends('layouts.master')

@section('title','Data Peserta')

@section('css')
    <!-- DataTables -->
    <link href="{{asset('admin/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Responsive datatable examples -->
    <link href="{{asset('admin/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Laporan Kunjungan</h4>
                    <br>

                    <div class="table-responsive">

                        <table id="tableAbsen" class="table table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Foto Kunjungan</th>
                                <th>Tgl Kunjungan</th>
                                <th>Tempat Kunjungan</th>
                                <th>Jumlah Peserta</th>
                            </tr>
                            </thead>
                            <tbody class="table-striped">
                            @php $no = 1; @endphp
                            @foreach($absensi as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>
                                        <a target="_blank" href="{{ asset('storage/'.$row->foto) }}"><img class="d-flex mr-5 img-thumbnail thumb-lg" src="@if($row->foto != 'default.png'){{ asset('storage/'.$row->foto) }} @else {{ asset('admin/images/users/default.png') }} @endif" alt="Poto Profil"></a>
                                    </td>
                                    <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->tgl)->format('j F Y, g:i A') }}</td>
                                    <td>{{ $row->tempat }}</td>
                                    <td>{{ $row->jumlah }} peserta</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                    <!-- Modal Absensi -->
                    <hr>
                    <div class="modal fade" id="kunjungan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog  modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Silahkan Isi Kunjungan Berikut
                                        ini..</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('kunjungan.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="judul">Tempat Kunjungan</label>
                                            <input type="text" maxlength="191" class="form-control" name="kunjungan"
                                                   id="kunjungan" required="" placeholder="Masukkan Data Kunjungan">
                                        </div>
                                        <div class="form-group">
                                            <label for="judul">Jumlah Peserta</label>
                                            <input type="number" maxlength="191" class="form-control" name="jumlah"
                                                   id="jumlah" required="" placeholder="Jumlah Peserta">
                                        </div>

                                        <div class="form-group">
                                            <label for="path">Foto Hasil Kunjungan</label><br>
                                            <input type="file" class="filestyle" data-input="false"
                                                   data-buttonname="btn-secondary btn-sm" name="foto" id="path"
                                                   accept="image/*">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup
                                        </button>
                                        <button type="submit" class="btn btn-primary">Hadir</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@stop

@section('footer')
    <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"
            type="text/javascript"></script>
    {{--    <script src="https://code.jquery.com/jquery-3.5.1.js" type="text/javascript"></script>--}}
    <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js" type="text/javascript"></script>
    <script>
        $('#tableAbsen').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );
        @if(Session::has('kosong'))
        alertify.error("Data Gambar tidak boleh kosong!");
        @endif
    </script>
    <script>
        @if(Session::has('gagal'))
        alertify.error("Anda sudah melakukan kunjungan hari ini!");
        @endif
    </script>
    <script>
        @if(Session::has('sukses'))
        alertify.success("Berhasil melakukan laporan kunjungan!");
        @endif
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>

    <script>
        $().DataTable();
    </script>
@stop
