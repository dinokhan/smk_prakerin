@extends('layouts.master')

@section('title','Dashboard Peserta')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">
@stop
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">
                        Laporan Absensi Harian Siswa {{ auth()->user()->nama_lengkap }}
                    </h4>
                    <br>
                    <table id="tableAbsen" class="table table-striped">
                        <thead>
                        <tr>
                            <th width="10">No</th>
                            <th>Tanggal</th>
                            <th>Kunjungan</th>
                            <th>Keterangan</th>
                            <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody class="table-striped">
                        @php $no = 1; @endphp
                        @foreach($kehadiran as $row)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->format('j F Y, g:i A') }}</td>
                                <td>{{ $row->kunjungan }}</td>
                                @if($row->status  == 'hadir')
                                    <td>
                                        <button class="btn btn-success btn-sm">Hadir</button>
                                    </td>
                                @elseif($row->status  == 'absen')
                                    <td>
                                        <button class="btn btn-danger btn-sm">Tidak Hadir</button>
                                    </td>
                                @elseif($row->status  == 'izin')
                                    <td>
                                        <button class="btn btn-warning btn-sm">Izin</button>
                                    </td>
                                @endif
                                <td>
                                    <button class="btn btn-success" data-toggle="modal"
                                            data-target="#detail-{{ $row->id }}"><i class="fa fa-eye"></i></button>
                                </td>
                            </tr>
                            <!-- Modal Detail -->
                            <div class="modal fade" id="detail-{{ $row->id }}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog  modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Absensi Harian</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @if($row->status  == 'hadir')
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="judul">Tempat Kunjungan</label>
                                                    <p>{{ $row->kunjungan }}</p>

                                                </div>

                                                <div class="form-group">
                                                    <label for="artikel">Rangkuman Hasil Kunjungan </label>
                                                    <p>{{ $row->rangkuman }}</p>

                                                </div>

                                                <div class="form-group">
                                                    <label for="path">Foto Hasil Kunjungan</label>
                                                    <img class="d-flex mr-5 img-thumbnail " src="@if($row->foto != 'default.png'){{ asset('storage/'.$row->foto) }} @else {{ asset('admin/images/users/default.png') }} @endif" alt="Poto Profil">

                                                </div>
                                            </div>
                                        @elseif($row->status  == 'izin')
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="artikel">Alasan izin </label>
                                                    <p>{{ $row->rangkuman }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="path">Bukti Izin</label>
                                                    <img class="d-flex mr-5 img-thumbnail " src="@if($row->foto != 'default.png'){{ asset('storage/'.$row->foto) }} @else {{ asset('admin/images/users/default.png') }} @endif" alt="Poto Profil">

                                                </div>
                                            </div>
                                        @endif
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>
                    <hr>
                    <br>

                    <!-- Modal Absensi -->
                    <div class="modal fade" id="absensi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog  modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Silahkan Isi Kehadiran Berikut
                                        ini..</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('absen.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf

                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="judul">Tempat Kunjungan</label>
                                            <input type="text" maxlength="191" class="form-control" name="kunjungan"
                                                   id="kunjungan" required="" placeholder="Masukkan Data Kunjungan">
                                        </div>

                                        <div class="form-group">
                                            <label for="artikel">Rangkuman Hasil Kunjungan </label>
                                            <textarea name="rangkuman" id="rangkuman" class="summernote form-control"
                                                      required=""></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="path">Foto Hasil Kunjungan</label>
                                            <input type="file" class="filestyle" data-input="false"
                                                   data-buttonname="btn-secondary btn-sm" name="foto" id="path"
                                                   accept="image/*">
                                            <code class="highlighter-rouge" style="color: red;">*Wajib diisi, jika tidak
                                                maka dianggap absen</code>
                                        </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup
                                        </button>
                                        <button type="submit" class="btn btn-primary">Hadir</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Modal izin -->
                    <div class="modal fade" id="izin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog  modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Silahkan Isi Data Berikut ini..</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('absen.izin') }}" method="POST" enctype="multipart/form-data">
                                    @csrf

                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="artikel">Alasan izin </label>
                                            <textarea name="rangkuman" id="rangkuman" class="summernote form-control"
                                                      required=""></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="path">Bukti Izin</label>
                                            <input type="file" class="filestyle" data-input="false"
                                                   data-buttonname="btn-secondary btn-sm" name="foto" accept="image/*">
                                            <code class="highlighter-rouge" style="color: red;">*Wajib diisi, jika tidak
                                                maka dianggap absen</code>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup
                                        </button>
                                        <button type="submit" class="btn btn-primary">Hadir</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


@stop

@section('footer')

    <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"
            type="text/javascript"></script>
{{--    <script src="https://code.jquery.com/jquery-3.5.1.js" type="text/javascript"></script>--}}
    <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js" type="text/javascript"></script>

    <script src="{{asset('admin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('admin/plugins/parsleyjs/parsley.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#tableAbsen').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );
        } );
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>
    <script>
        @if(Session::has('gagal'))
        alertify.error("Anda sudah melakukan absensi!");
        @endif
    </script>
    <script>
        @if(Session::has('kosong'))
        alertify.error("Data Gambar tidak boleh kosong!");
        @endif
    </script>
    <script>
        @if(Session::has('sukses'))
        alertify.success("Berhasil melakukan absensi!");
        @endif
    </script>

@stop
