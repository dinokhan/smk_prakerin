@extends('layouts.master')

@section('title','Data Peserta')

@section('css')
    <!-- DataTables -->
    <link href="{{asset('admin/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Responsive datatable examples -->
    <link href="{{asset('admin/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css"/>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Data Peserta</h4>
                    <br>
                    <div class="table-responsive">
                        <table id="datatable" class="table table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>NIS</th>
                                <th>Nama Lengkap</th>
                                <th>JK</th>
                                <th>Kelas</th>
                                <th>Tempat Prakerin</th>
                                <th>Nilai Akhir</th>
                                <th>Data Kehadiran</th>
                            </tr>
                            </thead>
                            <tbody class="table-striped">
                            @forelse($neko as $jquin)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><a href="{{ route('pesertap.nilai', $jquin->nis) }}" class="text-info"
                                           data-toggle="tooltip" data-placement="top"
                                           title="Detail Peserta">{{ $jquin->nis }}</a></td>
                                    <td>{{ $jquin->nama_lengkap }}</td>
                                    <td>
                                        @if($jquin->jk == 'L')
                                            Laki-Laki
                                        @else
                                            Perempuan
                                        @endif
                                    </td>
                                    <td>{{ $jquin->grup->kelas }}</td>
                                    <td>{{ $jquin->perusahaan->nama_perusahaan }}</td>
                                    {{-- Hitung Nilai Akhir --}}
                                    @php
                                        $nilaiJ = $jquin->nilai_jurnal;
                                        $nilaiP = $jquin->nilai_presentasi;
                                        $total = $nilaiJ + $nilaiP;
                                        $hasil = $total/2;
                                    @endphp
                                    <td>{{ $hasil }}</td>
                                    <td class="text-center">
                                        <button class="btn btn-success" data-toggle="modal"
                                                data-target="#detail-{{ $jquin->user_id }}"><i class="fa fa-eye"></i>
                                        </button>
                                    </td>
                                </tr>
                                <!-- Modal Detail -->

                            @empty
                                <tr>
                                    <td colspan="7"><b><i>Tidak Ada Data</i></b></td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        @foreach($neko as $jquin)
                        <div class="modal fade" id="detail-{{ $jquin->user_id }}" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog  modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Absensi ( {{ $jquin->nama_lengkap }} )</h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <table id="tableAbsen" class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th width="10">No</th>
                                                <th>Tanggal</th>
                                                <th>Kunjungan</th>
                                                <th>Keterangan</th>
                                                <th>Opsi</th>
                                            </tr>
                                            </thead>
                                            <tbody class="table-striped">
                                            @php $no = 1; @endphp
                                            @foreach($kehadiran as $row)
                                                @if($row->student_id  == $jquin->user_id)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->format('j F Y, g:i A') }}</td>
                                                        <td>{{ $row->kunjungan }}</td>
                                                        @if($row->status  == 'hadir')
                                                            <td>
                                                                <button class="btn btn-success btn-sm">Hadir
                                                                </button>
                                                            </td>
                                                        @elseif($row->status  == 'absen')
                                                            <td>
                                                                <button class="btn btn-danger btn-sm">Tidak
                                                                    Hadir
                                                                </button>
                                                            </td>
                                                        @elseif($row->status  == 'izin')
                                                            <td>
                                                                <button class="btn btn-warning btn-sm">Izin
                                                                </button>
                                                            </td>
                                                        @endif
                                                        <td>
                                                            <button class="btn btn-primary" data-toggle="modal"
                                                                    data-target="#lihat-{{ $row->id }}"><i
                                                                    class="fa fa-file"></i></button>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Tutup
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    @foreach($kehadiran as $row)
                        <!-- Modal Detail -->
                            <div class="modal fade" id="lihat-{{ $row->id }}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog  modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Detail Laporan P</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @if($row->status  == 'hadir')
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="judul">Tempat Kunjungan</label>
                                                    <p>{{ $row->kunjungan }}</p>

                                                </div>

                                                <div class="form-group">
                                                    <label for="artikel">Rangkuman Hasil Kunjungan </label>
                                                    <p>{{ $row->rangkuman }}</p>

                                                </div>

                                                <div class="form-group">
                                                    <label for="path">Foto Hasil Kunjungan</label>
                                                    <img class="d-flex mr-5 img-thumbnail "
                                                         src="@if($row->foto != NULL){{ asset('storage/'.$row->foto) }} @else {{ asset('admin/images/users/default.png') }} @endif"
                                                         alt="Poto Profil">

                                                </div>
                                            </div>
                                        @elseif($row->status  == 'izin')
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="artikel">Alasan izin </label>
                                                    <p>{{ $row->rangkuman }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="path">Bukti Izin</label>
                                                    <img class="d-flex mr-5 img-thumbnail "
                                                         src="@if(auth()->user()->path != 'default.png'){{ asset('storage/'.$row->foto) }} @else {{ asset('admin/images/users/default.png') }} @endif"
                                                         alt="Poto Profil">

                                                </div>
                                            </div>
                                        @endif
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@stop

@section('footer')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('admin/pages/datatables.init.js')}}"></script>
    <!-- Parsley js -->
    <script src="{{asset('admin/plugins/parsleyjs/parsley.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>

    <script>
        $().DataTable();
    </script>
@stop
